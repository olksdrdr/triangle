def is_triangle(a, b, c):
    '''
    please add your solution here or call your solution implemented in different function from here  
    then change return value from 'False' to value that will be returned by your solution
    '''
    if (a + b <= c) or (b + c <= a) or (a + c <= b): 
        return False
    else: 
        return True